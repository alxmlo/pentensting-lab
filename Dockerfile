FROM kalilinux/kali-rolling
RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install -y metasploit-framework \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
