# Pentensting Lab

Laboratorio para pruebas de penetración.

## Requerimientos

* Git
* Docker

## Play with Docker

Play with Docker (PwD) es un simple, interactivo y divertido entorno para aprender Docker.

1. Crear una cuenta en [Docker Hub](https://hub.docker.com/) 
2. Utilizar `docker` como metodo de autenticación en [PwD](https://labs.play-with-docker.com/)

## Docker Windows
## Docker MacOs
## Docker Linux

## Instalación

```sh
git clone https://gitlab.com/alxmlo/pentensting-lab.git && cd pentesting-lab
```

## Contenedores

* **kali**
* **dvwa** `:8081`
* **webgoat** `:8082`
* **bwapp** `:8083`
* **juice-shop** `:8084`
* **mutillidae** `:8085`

### Kali

```sh
docker-compose up -d kali && docker exec -it kali /bin/bash
```

### DVWAP

```sh
docker-compose up -d dvwa
```

* [Login](http://localhost:8081/login.php) Username: admin Password: password
* [Setup](http://localhost:8081/setup.php) Create / Reset database
* [Dificulty](http://localhost:8081/security.php) DVWA Security - Security Level

### OWASP WebGoat

```sh
docker-compose up -d webgoat
```

* [Login](http://localhost:8082/)

### bWapp

```sh
docker-compose up -d bwapp
```

* [Install](http://localhost:8083/install.php)
* [Login](http://localhost:8083/login.php) Username: bee Password: bug
* [Portal](http://localhost:8083/portal.php)

### Juicie Shop

```sh
docker-compose up -d juice-shop
```

* [Home](http://localhost:8084/)

### OWASP Mutillidae II

```sh
docker-compose up -d juice-shop
```

* [Home](http://localhost:8085/)

## Referencias

* [Un informático del lado del mal - Cómo montar un entorno de pentesting desde cero con Docker - Parte 1](https://www.elladodelmal.com/2018/09/como-montar-un-entorno-de-pentesting.html)
* [Un informático del lado del mal - Cómo montar un entorno de pentesting desde cero con Docker - Parte 1](https://www.elladodelmal.com/2018/09/como-montar-un-entorno-de-pentesting_14.html)
* [GitHub - Moises Tapia - Pentestening Lab](https://github.com/MoisesTapia/Pentesting-lab)
* [Hacking Articles - Web Application Pentest Lab setup Using Docker](https://www.hackingarticles.in/web-application-pentest-lab-setup-using-docker/)
*[Hackernoon - How To Dockerize Your Pen-testing Lab [feat. Kali Linux]](https://hackernoon.com/automating-your-pentest-lab-with-docker-2j9e33wa)
